package rajeeva;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.io.FileNotFoundException;
public class ExceptionHandleDemo {

	public static void main(String[] args) {
		File fileObj = new File("C:\\Users\\241567\\Documents");
		  FileReader fr = null;
		  try
		  {
		      
		      fr = new FileReader(fileObj);
		  }
		  catch(FileNotFoundException fe)
		  {
		      System.out.println("Inside the catch block of FileNotFoundException");
		      fe.printStackTrace();
		  }
		  int rd;  
		  try
		  {
		      
		      while( (rd = fr.read()) != -1)  
		      { 
		       
		               System.out.print( (char) rd);
		      }
		      fr.close();
		  }
		  
		  catch(IOException | NullPointerException ex)
		  {
		      System.out.println("Inside the catch block of NullPointerException & IOException");
		      ex.printStackTrace();
		  }
		}
		

	}


