package assignments;
import java.util.*;
public class ArrayProgram {

	public static void main(String[] args) {
	     Scanner input = new Scanner(System.in);
         int size;
         double fnum = 0, snum = 0;
         System.out.println("Enter the size of an array: ");
         size= input.nextInt();
         double[] array = new double[size];
         for(int i=0;i<size;i++) {
        	 System.out.println("Enter the array elements "+(i+1)+":");
        	 array[i]= input.nextDouble();
             
        	 if(fnum<array[i]) {
        		 snum=fnum;
        		 fnum=array[i];
        	 }
        	 else if(snum<array[i]) {
        		 snum=array[i];
        	 } 
        	  
         }
         for(double d:array) {
       	  System.out.println(d);
         }
              System.out.println(snum+"is the second largest number in an array");
	          input.close();
	}

}
