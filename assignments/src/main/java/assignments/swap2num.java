package assignments;
import java.util.*;
public class swap2num {

	public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num1=0,num2=0;
        System.out.println("Enter the value of num1: ");
        num1=input.nextInt();
        System.out.println("Enter the value of num2: ");
        num2=input.nextInt();
        System.out.println("Before swaping: "+num1+" "+num2);
        int temp=0;
        temp=num1;
        num1=num2;
        num2=temp;
        System.out.println("After swaping: "+num1+" "+num2);
        input.close();
        
	}

}
