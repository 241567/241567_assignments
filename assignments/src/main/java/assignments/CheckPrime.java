package assignments;
import java.util.*;
public class CheckPrime {

	public static void main(String[] args) {
		Scanner sc= new Scanner(System.in);
		int temp;
		boolean isPrime=true;
		int num;
		System.out.println("Enter the number:");
		num = sc.nextInt();
		for(int i=2;i<=num/2;i++) {
			temp=num%2;
			if(temp==0) {
				isPrime = false;
			}
		}
		 if(isPrime) {
			 System.out.println(num+ " is prime number"); 
		 }
		 else {
			 System.out.println(num+ " not prime number");
		 }
		  sc.close();
		
	}

}
