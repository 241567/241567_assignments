package assignments;

import java.util.*;

public class ItterateArrayList {
      
			public static void  main(String[] args) {
				List<Integer>mylist=new ArrayList<Integer>();
				Scanner input = new Scanner (System.in);	
	        	 for(int i=0;i<5;i++) {
	        		 mylist.add(input.nextInt());
	        	 } 
				  System.out.println("For Loop");
				  for(int counter=0;counter<mylist.size();counter++) {
					  System.out.println(mylist.get(counter));
				  }
				  System.out.println("While Loop");
				  int count=0;
				  while(mylist.size()>count) {
					  System.out.println(mylist.get(count));
					  count++;
				  }
				  System.out.println("Advanced For Loop");
				  for(Integer num : mylist) {
					  System.out.println(num);
				  }
				  input.close();
			}  
			     
        
}
