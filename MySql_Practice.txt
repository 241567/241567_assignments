CREATE TABLE `workers` (
  `worker_Id` varchar(10) NOT NULL,
  `worker_Name` varchar(15) DEFAULT NULL,
  `accno` varchar(10) DEFAULT NULL,
  `bal` decimal(10,2) DEFAULT NULL,
  `mobile` bigint DEFAULT NULL,
  PRIMARY KEY (`worker_Id`)
);

1) select * from workers;

2) insert into workers (worker_ID,worker_Name,accno,bal,contact)values("01A08","Suman",20151188,57000.18,9480048364);

3)ALTER TABLE workers
RENAME COLUMN contact TO mobile;

4) SELECT  worker_Name,bal FROM workers
ORDER BY worker_Name asc;

5) SELECT worker_Id,mobile FROM workers
WHERE worker_Name = 'Suman' AND bal = 380000;

6) SELECT worker_ID FROM workers
WHERE NOT worker_Id BETWEEN '01A01' AND '01A03';

7)SELECT worker_Name ,bal,mobile FROM workers
WHERE worker_Name LIKE 'A%';